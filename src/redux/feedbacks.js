import * as ActionTypes from './ActionTypes';

export const Feedbacks = (state = {isLoading: true, errMess: null, users: []}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_FEEDBACK:
            var feedback = action.payload;
            return {...state, isLoading: false, errMess:null, feedbacks: state.feedbacks.concat(feedback)};

        case ActionTypes.FEEDBACK_LOADING:
            return {...state, isLoading: true, errMess: null, feedbacks:[]};

        case ActionTypes.FEEDBACK_FAILED:
            return {...state, isLoading: false, errMess:action.payload}    

        default:
          return state;
      }
};
