import {createStore, combineReducers, applyMiddleware } from 'redux';
import { Feedbacks } from './feedbacks';
import { createForms } from 'react-redux-form';
import { InitialFeedback } from './form';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            feedbacks: Feedbacks,
            ...createForms({
                feedback: InitialFeedback
            })
        }),
        applyMiddleware(thunk, logger)
        
    );

    return store;
}