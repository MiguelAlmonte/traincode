import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';


export const addFeedback = (feedback) => ({
    type: ActionTypes.ADD_FEEDBACK,
    payload: feedback
    
});

export const postFeedback = (firstname, lastname, telephone, email, message) => (dispatch) => {
    const newFeedback = {
      firstname: firstname,
      lastname: lastname,
      telephone: telephone,
      email: email,
      message: message
    };
    newFeedback.date = new Date().toISOString();
    newFeedback.id = Math.floor(Math.random() * 100);

    return fetch(baseUrl + 'feedback', {
      method: "POST",
      body: JSON.stringify(newFeedback),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    })
    .then(response =>{
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
      throw error;
    })
    .then(response => response.json())
    //.then(response => dispatch(addFeedback(response)))
    .catch(error =>  { console.log('post feedback', error.message); alert('Your comment could not be posted\nError: '+error.message); });
};