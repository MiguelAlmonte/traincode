import React, { Component } from 'react';
import Contact from './ContactComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { actions } from 'react-redux-form';
import { postFeedback} from '../redux/ActionCreators';

const mapStateToProps = state => {
    return {
      feedbacks: state.feedbacks,
    }
  }

const mapDispatchToProps = dispatch => ({
  
    postFeedback: (firstname, lastname, telephone, email, message) => dispatch(postFeedback(firstname, lastname, telephone, email, message)),
    resetFeedbackForm: () => { dispatch(actions.reset('feedback'))}
});  

class Main extends Component {

  constructor(props) {
    super(props);
    console.log("Main const"); 
  }

  render() {
    const HomePage = () => {
        return(
            <Contact postFeedback={this.props.postFeedback} resetFeedbackForm={this.props.resetFeedbackForm}/>
        );
      }
    return (
      <div>
        <Header />
        <Switch>
        <Route path='/home' component={HomePage} />
        <Redirect to="/home" />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));